# Spip2Spip

![Spip2Spip](./prive/themes/spip/images/spip2spip-xx.svg)

Spip2spip permet de synchroniser le contenu de plusieurs sites SPIP entre eux en étendant le principe de la syndication thématique. 

Les articles d’un SPIP sont recopiés d’un site à l’autre en conservant leur formatage original. 

## Documentation

https://contrib.spip.net/1517

# Changelog


## [4.1.2] 2024-01-29

### Fixed

- #5 Supprimer l'auteur en trop

## [4.0.2] 2023-07-18

### Fixed

- Pagination SPIP 4

### Changed

- Compatibilité SPIP 4.2
- Ajout d'un CHANGELOG.md et README.md